###########################
#### Configuracion DB #####
###########################
> Install adaptador DB
```
/d/wamp64/bin/php/php7.1.9/php.exe composer.phar require zendframework/zend-db
```

> check \config\config.php
```
$aggregator = new ConfigAggregator([
   \Zend\Db\ConfigProvider::class
])
```

> create \config\autoload\db.global.php
```
return [
    'db' => [
        'driver' => 'Pdo',
        'dsn'      => 'mysql:dbname=siar_db;host=127.0.0.1;port=3306;',
        'user'     => 'root',
        'password' => ''
    ],
];
```